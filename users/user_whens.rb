When(/^I create my user by entering my user details$/) do
  fill_in "user_email", with: @user.email
	fill_in "user_password", with: @user.password
	fill_in "user_password_confirmation", with: @user.password
	click_button "Sign up" 

end

When(/^I sign in with my user details with the "([^"]*)" password$/) do |password_type|
	fill_in "user_email", with: @user.email	

	case password_type
	when "correct" then fill_in "user_password", with: @user.password
	when "incorrect" then fill_in "user_password", with: "#{@user.password}*wrongone"
	end
  
	click_button "Log in"
  end

	When("I update the {string} on the account page") do |update_type|

		case update_type
		when 'user password'
			new_email_address = @user.email
			new_password = Faker::Internet.password
		when 'email address'
			new_email_address = Faker::Internet.email
			new_password = @user.password
		end
		
		fill_in "user_email", with: new_email_address
		fill_in "user_password", with: new_password
		fill_in "user_password_confirmation", with: new_password
		fill_in "user_current_password", with: @user.password
	
		click_button "Update"
	
		@user.email = new_email_address
		@user.password = new_password
	
	end
	

When('I fill in the {string} profile I have in mind') do |string|
	fill_in "user_attribute_username", with: @user_attribute.username
	fill_in "user_attribute_name", with: @user_attribute.name
end

When('I fill in my {string} {string} in mind in the {string} {string} selector') do 
	|sub_resource_name, sub_resource_property, selector_name, selector_type|

	fill_in_value = @user.send(proper_object_name(sub_resource_name)).send(proper_object_name(sub_resource_property))
	fill_in proper_selector_name(selector_name, selector_type), with: fill_in_value
end

When('I fill in my {string} in the {string} {string} selector') do 
	|property_name, selector_name, selector_type|

	fill_in_value = @user.send(proper_object_name(property_name))
	fill_in proper_selector_name(selector_name, selector_type), with: fill_in_value
end