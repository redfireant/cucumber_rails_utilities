require 'faker'

Given(/^I have (?:a|an) "([^"]*)" user in mind$/) do |user_type|
    case user_type
    when "general" 
        @user = FactoryBot.build(:general_user)
    when "administrator"
        @user = FactoryBot.build(:administrator_user)
    when "default general"
        @user = FactoryBot.build(:general_user, email: "admin@mycompany.com", password: "Password*01")
    end
end

Given(/^I am (?:a|an) "([^"]*)" user in the system$/) do |user_type|

    @user = FactoryBot.create(:user)

    case user_type.downcase
    when "administrator"
        # @user = FactoryBot.create(:administrator_user)
        # @user.remove_role :user
        @user.add_role :administrator
    end

    if ENV['TEMPLATE_VERSION'].to_i < 7
        if user_type.downcase != 'no profile'
            @user_attribute = FactoryBot.create(:user_attribute, username: "#{user_type}_user", user_id: @user.id)
        end
    end
    
end

Given("there are {int} {string} users in the system") do |num_of_users, user_type|
    @users = []

    (0..num_of_users.to_i - 1).each do
        system_user = FactoryBot.create("#{user_type.to_s}_user", email: Faker::Internet.email)
        
        if user_type != 'general'
            system_user.add_role :"#{user_type}"
        end

        @users << system_user
    end

end

Given(/^I am signed out of the system$/) do
    delete destroy_user_session_path
end

Given(/^I am signed in to the system$/) do
    visit sign_in_path
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: @user.password
    click_button "Log in"
end

Given('I have a {string} profile in mind') do |string|
    @user_attribute = FactoryBot.build(:user_attribute)
end    

Given('the user profile is setup') do
    @user_attribute = @user.user_attribute
end

Given('the User {int} is associated to the {string} role') do |user_number, role_name|

    user = generic_assign_variable('user', user_number)
    user.add_role :"#{role_name.downcase}"

end
    