#
# The general thens steps provide the user verifications for the solution #
#

Then(/^I will see my "(.*?)" on the "(.*?)" "(.*?)"$/) do |fieldname, elementname, elementtype|
  within "##{elementname.downcase}-#{elementtype.downcase}" do
    page.should have_text(@user.email)
  end
end

Then(/^I can see all (\d+) users on the page$/) do |num_of_users|

  for user_number in 0..(num_of_users.to_i - 1)
    within "#user-id-#{@users[user_number].id}" do
      page.should have_text(@users[user_number].email)
    end
  end

end

Then('I {string} see my {string} in the {string} {string} selector on the page') do 
  |intention_type, user_field, selector_name, selector_type|

  locate_user = generic_assign_variable('my',0,false)
  compare_string = locate_user.send(proper_object_name(user_field)) 

  check_for_values_within_section(intention_type, compare_string, selector_name, selector_type)
end

Then('I {string} see my {string} {string} in the {string} {string} selector') do 
  |intention_type, user_sub_object, user_sub_object_field, selector_name, selector_type|

  locate_user = generic_assign_variable('my',0,false)

  compare_string = locate_user.send(proper_object_name(user_sub_object)).send(proper_object_name(user_sub_object_field))

  check_for_values_within_section(intention_type, compare_string, selector_name, selector_type)
end