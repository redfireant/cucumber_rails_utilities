When('I fill in the {string} in mind {string} in the {string} {string} selector/field') do
  |resource_name, resource_field, selector_name, selector_type|

  locate_resource = generic_assign_variable(resource_name, 0, true)

  fill_value = locate_resource.send(:"#{proper_object_name(resource_field)}")

  fill_in_value fill_value, selector_name

end

When('I populate the {string} basic information I have in mind') do |resource_name|
  locate_resource = generic_assign_variable(resource_name,0,true)
  fill_in proper_selector_name("#{resource_name} name", 'selector'), with: locate_resource.name
  fill_in proper_selector_name("#{resource_name} description", 'selector'), with: locate_resource.description

end

When('I populate the {string} basic information I have in mind in the subform of {string}') do
  |resource_name, parent_form_name|

  locate_resource = generic_assign_variable(resource_name,0,true)
  field_prefix = "#{parent_form_name} #{resource_name.pluralize}"
  fill_in proper_selector_name("#{field_prefix} name", 'selector'), with: locate_resource.name
  fill_in proper_selector_name("#{field_prefix} description", 'selector'), with: locate_resource.description

end

When('I click on the {string} in mind {string}') do |resource_name, link_name|

  locate_resource = generic_assign_variable(resource_name,0,true)

  object_find = resource_name.camelcase.constantize.find_by(name: locate_resource.name)

  link_name = proper_selector_name("#{resource_name} #{object_find.id} #{link_name}", 'id')

  page.find(:css, "#{link_name}").click
end

When('I select {string} in mind {string} from the {string} on the page') do
  |resource_name, resource_field, selector_name|

  locate_resource = generic_assign_variable(resource_name,0,true)

  select_value = locate_resource.send(:"#{proper_object_name(resource_field)}")

  page_element_name = proper_selector_name(selector_name, 'id')

  find("select#{page_element_name} option", text: select_value).select_option

end
