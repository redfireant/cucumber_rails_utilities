Given('I have a(n) {string} in mind') do |object_name|
  formal_object_name = object_name.singularize.downcase.gsub(/ /,'_')
  instance_variable_set("@#{formal_object_name}",FactoryBot.build(:"#{formal_object_name}"))
end

Given('the {string} in mind {string} value has the {string} value/property of {string}') do 
  |resource_name, resource_property, value_type, value|

  locate_resource = generic_assign_variable(resource_name, 0, true)

  assign_value_to_object_property(locate_resource, resource_property, value_type, value)

end