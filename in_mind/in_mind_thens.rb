# Check for values for objects in mind

Then('I will see the {string} in mind {string} value in the {string} {string} selector( on the page)') do 
  |resource_name, property_value, selector_name, selector_type|

  resource = generic_assign_variable(resource_name, 0, true)

  check_value = value_translate(resource.send(:"#{property_value.downcase.gsub(/ /,'_')}"))
  
  within proper_selector_name(selector_name, selector_type) do
    expect(page).to have_text(check_value)
  end

end

Then('I will be on the {string} in mind {string} page') do |resource_name, page_type|

  locate_resource = generic_assign_variable(resource_name, 0, true)

  page_check(locate_resource, page_type)

end

Then("I can click on the {string} in mind {string} in the {string} on the page") do 
  |resource_name, resource_field, page_element_id|

  resource = generic_assign_variable(resource_name, 0, true)
  compare_value = resource.send(proper_object_name(resource_field))
  
  expect(page).to have_selector(proper_selector_name(page_element_id,'id') , text: compare_value)
  
end
