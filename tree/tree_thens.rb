Then('I will see all {string} {int} descendants in the {string} {string} selector') do |object_name, object_id, selector_name, selector_type|

  within proper_selector_name(selector_name, selector_type) do
    resource_object = generic_assign_variable(object_name, object_id)

    resource_object.descendants.each do |resource_children|
      expect(body).to have_text(resource_children.name)
    end
    
  end
end
  