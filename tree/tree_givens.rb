Given('there are {int} children objects associated to {string} {int} in the system') do |num_of_children, resource_name, resource_number|

  resource_object = generic_assign_variable(resource_name, resource_number)

  if resource_name.downcase.include? ' child'
    formal_object_name = resource_name.downcase.gsub(/ child/,'').gsub(/ /,'_')
    children_object_name = "#{formal_object_name}_grandchildren"
  else
    formal_object_name = resource_name.singularize.downcase.gsub(/ /,'_')
    children_object_name = "#{formal_object_name}_children"
  end

  children_objects = instance_variable_get("@#{children_object_name}")

  if children_objects.nil? 
    children_objects = []
  end

  (0..(num_of_children.to_i - 1)).each do
    child_object =  FactoryBot.create(:"#{formal_object_name}", parent: resource_object)
    children_objects << child_object
  end

  instance_variable_set("@#{children_object_name}", children_objects)  

end

Given('there are {int} {string} nodes in the system') do |num_of_objects, resource_name|

  parent_node = create_parent_node(resource_name)

  local_array = associate_nodes_to_parents(num_of_objects, parent_node)
  instance_variable_set("@#{resource_name.pluralize.downcase.gsub(/ /,'_')}", local_array)  

end

Given('I have a root {string} node in the system') do |resource_name|
  parent_node = create_parent_node(resource_name)
end
