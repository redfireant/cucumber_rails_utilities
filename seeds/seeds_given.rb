require 'rake'

Given('I have the {string} seeded in the system') do |seed_name|
  full_seed_name = "db:seed:#{proper_object_name(seed_name)}"
  Rails.application.load_tasks
  Rake::Task[full_seed_name].invoke

end

Given('I have the {string} {string} seeded in the system') do |seed_category_name, seed_subcategory_name|

  full_seed_name = "db:seed:#{proper_object_name(seed_category_name)}:#{proper_object_name(seed_subcategory_name)}"
  Rails.application.load_tasks
  Rake::Task[full_seed_name].invoke

end