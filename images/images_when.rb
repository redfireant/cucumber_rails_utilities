When('I attach the {string} file to the {string} selector') do |file_name, selector_name|
  selector_name = proper_selector_name(selector_name, 'selector')
  attach_file(selector_name, "#{::Rails.root}/test/fixtures/files/#{file_name.downcase}")
end
