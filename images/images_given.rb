Given('there is an image associated with the {string} {int} {string} property') do |resource_name, resource_id, resource_property|

  full_image_name = "#{::Rails.root}/test/fixtures/files/logo_image.png"
  located_object = generic_assign_variable(resource_name, resource_id)
  located_object.send(:"#{resource_property.downcase.gsub(/ /,'_')}").attach(io: File.open(full_image_name), filename: 'logo_image.png', content_type: 'image/png')

end
