Then('the {string} image will be {int} px in width and {int} px in height') do |selector_name, width, height|

  tested_selector = proper_selector_name(selector_name, 'selector')
  image_height = page.evaluate_script("document.getElementById('#{tested_selector}').clientHeight")
  image_width = page.evaluate_script("document.getElementById('#{tested_selector}').clientWidth")

  expect(image_height).to be <= height, "Image height for #{tested_selector} was #{image_height}. Expected the height to be #{height}" 
  expect(image_width).to be <= width, "Image width for #{tested_selector} was #{image_width}. Expected the width to be #{width}" 

end