# Check to see string values on the page without objects
Then('I will see {string} in the hidden {string} field on the page') do |string_value, field_name|
  field_value = find_field(proper_selector_name(field_name, 'selector'), type: :hidden).value
  expect(field_value).to eq(string_value.downcase.gsub(/ /,'_'))
end

Then("I will see {string} in the title") do |value_name|
  expect(page).to have_title(value_name.to_s)
end

Then("I will see the value of {string} {string} in the {string} id") do |compare_string, compare_type, place_holder|

  case compare_type
  when 'configuration key'
    if ENV['TEMPLATE_VERSION'].to_i == 7
      place_value = ENV[proper_object_name(compare_string).upcase]
    else
      place_value = ENV[proper_object_name(compare_string).downcase]
    end
    
    expect(body).to have_css("##{place_holder.downcase.gsub(/ /,'_')}", text: place_value)
  else
    fail('Cannot find comparison type')
  end
end

Then("I will see( the value of) {string} in the {string} {string}( selector)") do |message, element, element_type|

  selector_name = proper_selector_name(element, element_type)
  expect(body).to have_selector(selector_name, text: message, exact_text: true)

end

Then('I {string} see {string} {string} in the {string} {string} selector') do 
  |intention_type, string_name, exactness, selector_name, selector_type|

  if exactness.downcase == 'exactly'
    exactness_flag = true
  else
    exactness_flag = false    
  end

  selector = proper_selector_name(selector_name, selector_type)
  expect(body).to have_selector(selector, text: string_name, exact_text: exactness_flag)

end


Then('I {string} see {string} within the {string} {string} selector') do 
  |intent_type, lookup_value, element_name, element_type|

  selector_name = proper_selector_name(element_name, element_type)

  within(selector_name) do
    case intent_type
    when 'will not', "won't" 
      expect(page).not_to have_content(lookup_value) 
    when 'will' 
      expect(page).to have_content(lookup_value)
    end

  end

end

Then("I will see {string} within the {string} {string}( selector)") do |message, element, element_type|

  selector_name = proper_selector_name(element, element_type)
  expect(body).to have_selector(selector_name, text: message)

end

Then("I {string} see the {string} {string} on the page") do |expectation_type, element_name, element_type|

  element = proper_selector_name(element_name, element_type)

  case expectation_type
  when 'will'
    expect(body).to have_css(element)
  when 'will not', "won't"
    expect(body).not_to have_css(element)
  else
    fail 'Invalid expectation type'
  end

end

Then('I will see the value of {string} in the {string} {int} {string} {string} selector') do 
  |compare_value, resource_name, resource_id, resource_property, selector_type|

  resource = generic_assign_variable(resource_name, resource_id)
  extracted_selector_name = extract_selector_name(resource_name)
  full_selector_name = proper_selector_name("#{extracted_selector_name} #{resource.id} #{resource_property}", selector_type)
  
  case selector_type.downcase
  when 'field'
    expect(body).to have_field(full_selector_name, with: compare_value)
  else
    expect(body).to have_selector(full_selector_name, text: compare_value)
  end

end

Then('I {string} see {string} in the {string} {int} {string} {string} selector within the {string} {string} selector') do 
  |intent_type, compare_text, within_resource_name, within_resource_id, within_object_property, within_selector_type, 
    element_name, element_type|

  within_object = generic_assign_variable(within_resource_name, within_resource_id)
  selector_name = "#{within_resource_name} #{within_object.id} #{within_object_property}"

  within proper_selector_name(element_name, element_type) do
    check_for_values_in_selectors(intent_type, compare_text, selector_name, within_selector_type)    
  end

end