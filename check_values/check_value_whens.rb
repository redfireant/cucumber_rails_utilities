When('I click on the link which has the {string} text') do |text_string|
  click_link(text_string, exact_text: true)
end

When("I select {string} from the {string} dropdown( on the page)") do |value_name, object_name|
  page.select value_name, from: "#{object_name.downcase.gsub(/ /,'_')}_dropdown"
end

When('I click the {string} for the {string} {string} of {string} {int}') do
  |link_descriptor, lookup_value, resource_property, resource_name, resource_number|
  
  main_object = generic_assign_variable(resource_name, resource_number)

  lookup_object = main_object.send(proper_object_name(resource_property.pluralize)).find_by_name(lookup_value)

  if lookup_object.nil?
    raise 'Could not lookup the value'
  else
    navigation_id = proper_selector_name("#{resource_property} #{lookup_object.id} #{link_descriptor}", 'id')
  end

  page.find(navigation_id).click
end

When('I select {string} from the {string} {string} selector from the {string}') do 
  |lookup_value, element_name, element_type, form_name|

  within(proper_selector_name(form_name, 'id')) do
    page.find(proper_selector_name(element_name, element_type)).select lookup_value
  end

end