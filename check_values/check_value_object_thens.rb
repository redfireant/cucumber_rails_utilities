Then('I {string} see {string} {string} {int} {string} value in the {string} {string} selector') do 
  |intent_type, display_text, resource_name, resource_id, resource_property, selector_name, selector_type|

  resource = generic_assign_variable(resource_name, resource_id)

  compare_value = "#{display_text} #{resource.send(proper_object_name(resource_property))}"

  check_for_values_within_section(intent_type, compare_value, selector_name, selector_type)

end

Then('I {string} see the {string} {int} {string} checkbox selector is {string}') do 
  |intent_type, resource_name, resource_id, resource_property, value|

  resource = generic_assign_variable(resource_name, resource_id)

  selector_name = proper_selector_name("#{resource_name} #{resource.id} #{resource_property}", 'id')

  if value.downcase == 'checked'
    expect(page.find(selector_name)).to be_checked
  else
    expect(page.find(selector_name)).not_to be_checked
  end

end