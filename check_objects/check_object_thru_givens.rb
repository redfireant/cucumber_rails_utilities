Given('there are {int} {string} associated with {string} {int} through {string} in the system') do 
  |num_of_child_objects, child_object, main_object, main_object_number, thru_object|
  
  child_resource_array = generic_resource_name(child_object.pluralize)
  child_resource_array = [] if child_resource_array.nil?

  thru_resource_array = generic_resource_name(thru_object.pluralize)
  thru_resource_array = [] if thru_resource_array.nil?

  main_resource = generic_assign_variable(main_object, main_object_number)

  case main_object.singularize
  when / child/
    proper_main_object_name = main_object.singularize.gsub(/ child/,'')
  when / grandchild/
    proper_main_object_name = main_object.singularize.gsub(/ grandchild/,'')
  else
    proper_main_object_name = main_object.singularize
  end

  (0..(num_of_child_objects.to_i - 1)).each do |counter|

    child_resource = FactoryBot.create(:"#{proper_object_name(child_object.singularize)}")
    thru_resource_array << FactoryBot.create(:"#{proper_object_name(thru_object.singularize)}", \
      :"#{proper_object_name(proper_main_object_name)}_id" => main_resource.id, \
      :"#{proper_object_name(child_object.singularize)}_id" => child_resource.id )
    child_resource_array << child_resource
  end


  instance_variable_set("@#{proper_object_name(child_object.pluralize)}", child_resource_array)
  instance_variable_set("@#{proper_object_name(thru_object.pluralize)}", thru_resource_array)
  
end

Given('{string} {int} is associated with {string} {int} through {string}') do 
  |associated_resource_name, associated_through_id, resource_name, resource_id, through_resource_name|

  associated_object = generic_assign_variable(associated_resource_name, associated_through_id)
  resource_object = generic_assign_variable(resource_name, resource_id)

  through_object = resource_object.send(:"#{proper_object_name(through_resource_name.pluralize)}").new
  through_object.send("#{proper_object_name(associated_resource_name)}=", associated_object)
  through_object.save

end

Given('{string} {int} is associated with {string} {int}') do 
  |associated_resource_name, associated_resource_id, resource_name, resource_id|

  associated_object = generic_assign_variable(associated_resource_name, associated_resource_id)
  resource_object = generic_assign_variable(resource_name, resource_id)

  resource_object.send("#{proper_object_name(associated_resource_name)}=", associated_object)
  resource_object.save

end

# Associating an existing object to another object
Given('{string} {int} is associated with {string} {int} {string} in the system') do 
  |resource_name, resource_id, parent_resource_name, parent_resource_id, parent_resource_field|

  local_object = generic_assign_variable(resource_name, resource_id)
  parent_object = generic_assign_variable(parent_resource_name, parent_resource_id)
  parent_object.send(parent_resource_field.downcase.gsub(/ /,'_')) << local_object
end

Given('there are {string} associated to all {string} in the system') do |child_object, parent_object|

  parent_resources = generic_resource_name(parent_object)

  child_resources = generic_resource_name(child_object)
  child_resources = [] if child_resources.nil?

  parent_resources.each do |parent_resource|
    child_resource = FactoryBot.create(:"#{child_object.singularize}")
    parent_resource.send("#{child_object.singularize}=", child_resource)
    parent_resource.save
    child_resources << child_resource 
  end

instance_variable_set("@#{proper_object_name(child_object.pluralize)}", child_resources)

end

Given('there are {string} associated to all {string} in the system through {string}') do 
  |child_object, parent_object, join_object|

  parent_resources = generic_resource_name(parent_object.pluralize)

  child_resources = generic_resource_name(child_object.pluralize)
  child_resources = [] if child_resources.nil?

  join_resources = generic_resource_name(join_object.pluralize)
  join_resources = [] if join_resources.nil?

  parent_resources.each do |parent_resource|
    child_resource = FactoryBot.create(:"#{ proper_object_name( child_object.singularize )}")
    join_resource = FactoryBot.create(:"#{ proper_object_name(join_object.singularize)}", "#{ proper_object_name(child_object.singularize) }_id": child_resource.id, 
      "#{ parent_object.singularize }_id": parent_resource.id)
    child_resources << child_resource 
    join_resources << join_resource
  end

  instance_variable_set("@#{ proper_object_name(join_object) }", join_resources)
  instance_variable_set("@#{ proper_object_name(parent_object) }", parent_resources)
  instance_variable_set("@#{ proper_object_name(child_object) }", child_resources)

end

# Creating a new entity through a has_many relationship
Given('{string} {int} has a new {string} {string} with a {string} value of {string}') do 
  |resource_name, resource_id, associated_resource_name, associated_resource_property, value_type, value|
  
  object = generic_assign_variable(resource_name, resource_id)
  associated_object = object.send(proper_object_name(associated_resource_name.pluralize)).new
  assign_value_to_object_property(associated_object, associated_resource_property, value_type, value)

  associated_object_list = generic_resource_name(associated_resource_name)

  if associated_object_list.nil?
    associated_object_list = []
  end

  associated_object_list << associated_object
  instance_variable_set("@#{proper_object_name(associated_resource_name.pluralize)}", associated_object_list)

end