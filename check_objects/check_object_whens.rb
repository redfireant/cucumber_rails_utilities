When('I fill in {string} {int} {string} in the {string} field') do 
  |resource_name, resource_id, resource_property_name, selector_name|

  locate_object = generic_assign_variable(resource_name, resource_id)
  compare_value_text = locate_object.send(proper_object_name(resource_property_name))

  fill_in proper_selector_name(selector_name, 'selector'), with: compare_value_text

end
  

When('I fill in {string} {int} {string} in the {string} field within the {string} {string}') do 
  |object_name, object_id, object_field_name, field_name, page_element_name, page_element_type|

  locate_resource = generic_assign_variable(object_name, object_id)
  compare_value_text = locate_resource.send(object_field_name.downcase.gsub(/ /,'_'))

  within proper_selector_name(page_element_name, page_element_type) do
    fill_in proper_selector_name(field_name, 'selector'), with: compare_value_text
  end

end

When('I click on the {string} {int} {string} in the {string} {string} section') do 
  |object_name, object_number, element_descriptor, selector_name, selector_type|

  resource = generic_assign_variable(object_name, object_number)
  navigation_id = proper_selector_name("#{object_name} #{resource.id} #{element_descriptor}", "id")

  within proper_selector_name(selector_name, selector_type) do
    page.find(navigation_id).click    
  end
end
