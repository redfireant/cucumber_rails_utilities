Then('I {string} see the {string} {int} {string}( value) in the {string} {string}( selector)( on the page)') do 
  |intent_type, object_name, object_id, object_field, selector_name, selector_type|

  resource_object = generic_assign_variable(object_name, object_id)

  compare_value = resource_object.send(proper_object_name(object_field))

  case compare_value.class.to_s
  when 'Date'
    compare_value = resource_object.send(proper_object_name(object_field)).strftime($date_format)
  end

  check_for_values_within_section(intent_type, compare_value, selector_name, selector_type)

end

Then('I {string} see the {string} {int} {string} on the page') do |expectation_type, resource_name, resource_number, field_name|

  resource = generic_assign_variable(resource_name, resource_number)

  case expectation_type.downcase
  when 'will'
    expect(page).to have_text(resource.send(:"#{field_name}"))
  when 'will not'
    expect(page).not_to have_text(resource.send(:"#{field_name}"))
  else
    raise "Invalid expection type. Expecting 'will' or 'will not' only"
  end

end