# Check for object values

Then('I {string} see the {string} {int} {string} {string} value in the {string} {int} {string} {string} selector') do 
  |intent_type, resource_name, resource_number, sub_resource_name, field_name, 
    selector_name, selector_id, selector_description, selector_type|

    resource_object = generic_assign_variable(resource_name, resource_number)
    full_selector_name = "#{selector_name} #{resource_object.id} #{selector_description}"
  
    compare_value = resource_object.send(:"#{sub_resource_name.downcase.gsub(/ /,'_')}").send(field_name.downcase.gsub(/ /,'_'))
    check_for_values_within_section(intent_type, compare_value, full_selector_name, selector_type)

end

Then('I {string} see the {string} {int} {string} {string} field in the {string} {int} {string} {string} selector in the {string} {string} selector') do
  |intent_type, resource_name, resource_number, sub_resource_name, field_name, 
    selector_name, selector_id, selector_description, selector_type,
    within_selector_name, within_selector_id|

  resource_object = generic_assign_variable(resource_name, resource_number)
  full_selector_name = "#{selector_name} #{resource_object.id} #{selector_description}"

  compare_value = resource_object.send(:"#{sub_resource_name.downcase.gsub(/ /,'_')}").send(field_name.downcase.gsub(/ /,'_'))

  within proper_selector_name(within_selector_name, within_selector_id) do
    check_for_values_within_section(intent_type, compare_value, full_selector_name, selector_type)
  end

end

Then('I {string} see the {string} {int} {string} field in the {string} {int} {string} {string} selector in the {string} {string} selector') do
  |intent_type, resource_name, resource_number, field_name, 
    selector_name, selector_id, selector_description, selector_type,
    within_selector_name, within_selector_id|

  resource_object = generic_assign_variable(resource_name, resource_number)
  full_selector_name = "#{selector_name} #{resource_object.id} #{selector_description}"

  compare_value = resource_object.send(field_name.downcase.gsub(/ /,'_'))

  within proper_selector_name(within_selector_name, within_selector_id) do
    check_for_values_within_section(intent_type, compare_value, full_selector_name, selector_type)
  end

end

Then('I {string} see the {string} {int} {string} value in the {string} {int} {string} {string} selector') do 
  |intent_type, check_resource_name, check_resource_id, check_resource_field, container_resource_name, container_resource_id, selector_name, selector_type|

  check_object = generic_assign_variable(check_resource_name, check_resource_id)
  container_object = generic_assign_variable(container_resource_name, container_resource_id)

  check_for_values_within_section(intent_type, check_object.send(:"#{proper_object_name(check_resource_field)}"), 
    "#{container_resource_name} #{container_object.id} #{selector_name}", selector_type )

end

Then('I {string} see the {string} {int} {string} {string} value in the {string} {string} selector on the page') do 
  |intent_type, resource_name, resource_id, associated_resource_name, associate_resource_property, selector_name, selector_id|

  resource_object = generic_assign_variable(resource_name, resource_id)

  compare_value = resource_id.send(proper_object_name(associated_resource_name)).send(proper_object_name(associated_resource_property))

  check_for_values_within_section(intent_type, compare_value, selector_name, selector_id)

end