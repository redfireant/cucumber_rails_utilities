Given('I have a {string} system configuration category with the value of {string}') do 
  |category, value|
  
  system_configured_category = SystemConfiguredCategory.find_by(name: category)

  if system_configured_category.nil?
    system_configured_category = FactoryBot.create(:system_configured_category, name: category)
  end

  system_configured_category = FactoryBot.create(:system_configured_value, system_configured_category: system_configured_category, name: value)

end

Given('the {string} {int} system configured {string} property has the value of {string}') do 
  |object_name, object_id, system_configuration_category, system_configuration_value|
  # Given('the {string} {float} system configured {string} property has the value of {string}') do |string, float, string2, string3|

  resource_object = generic_assign_variable(object_name, object_id)

  value = SystemConfiguredCategory.show_value(system_configuration_category, system_configuration_value).id

  resource_object.send("system_configured_#{proper_object_name(system_configuration_category)}_id=", value)
  resource_object.save


end

Given('I have the system configured categories {string} seeded in the system') do |category_name|

  seed_category_function_name = "load_#{proper_object_name(category_name.pluralize)}"
  DataLoader.send(:"#{seed_category_function_name}")

end