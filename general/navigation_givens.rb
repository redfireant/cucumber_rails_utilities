Given("I am on the {string} page") do |navigation|
  visit case navigation.downcase
    when 'home' then root_path
    when 'dashboard' then dashboard_path
    else send :"#{navigation.downcase.gsub(/ /,'_')}_path"
#    when /(.+) list$/ then send :"#{navigation.downcase.gsub(/ /,'_')}_path"
#    else fail('Undefined navigation path')
  end

end

Given("I am on the {string} {int} {string} page") do |resource_name, resource_id, page_type|

  locate_resource = generic_assign_variable(resource_name, resource_id)
  case page_type.downcase
  when 'show'
    visit send(:"#{locate_resource.class.name.underscore}_path", locate_resource.id)
  when 'edit'
    visit send(:"edit_#{locate_resource.class.name.underscore}_path", locate_resource.id)
  else
    fail 'Invalid page type.'
  end

end

Given('I am on my {string} page') do |path_name|
  visit "/users/#{@user.id}/#{path_name.downcase.gsub(/ /,'_')}"
end