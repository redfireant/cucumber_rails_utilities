
When(/^I click on the site logo$/) do
  page.find('.navbar-brand').click
end

When(/^I log out of the site$/) do
  page.find("#user-dropdown").click
  click_on "Sign out"
end

When(/^I go to the "([^"]*)" page$/) do |page_name|
  visit (send :"#{page_name.downcase.gsub(/ /, '_')}_path")
end

When('I click on the {string}') do |navigation_name|
  page.find("##{navigation_name.downcase.gsub(/ /,'_')}").click
end

When('I do a simple click on the {string} button') do |button_name|
  click_button button_name
end

When('I press on the confirmation button') do
  page.driver.browser.switch_to.alert.accept
end

When("I click on the pagy {string} button") do |pagy_button|
  within(:css, ".page-item.#{pagy_button}") do
    page.find(:css, ".page-link").click
  end
end

When("I click on the {string} {int} {string}") do |object_name, object_number, element_descriptor|

  locate_resource = generic_assign_variable(object_name,object_number)

  if object_name.downcase.include?('stuff')
    selector_name = proper_selector_name(object_name.singularize, 'id')
  else
    selector_name = proper_selector_name(object_name, 'id')
  end

  navigation_id = "#{selector_name}_#{locate_resource.id}_#{element_descriptor.downcase.gsub(/ /,'_')}"

  page.find(navigation_id).click

end

When('I click on the {string} {int} {string} in the {string} {string} on the page') do 
  |object_name, object_number, action_type, selector_name, selector_type|

  locate_resource = generic_assign_variable(object_name,object_number)

  navigation_id = proper_selector_name("#{object_name} #{locate_resource.id} #{action_type}", "id")

  within proper_selector_name(selector_name, selector_type) do
    page.find(navigation_id).click    
  end

end

When("I click on the {string} from position {int} {string} on the screen") do |resource_name, resource_position_number, link_name|
  resource = generic_assign_variable(resource_name,resource_position_number)

  navigation_id = "##{resource_name.downcase}_#{resource.id}_#{link_name.downcase.gsub(/ /,'_')}"

  page.find(navigation_id).click
end

When("I click on the {string} link in the {string} area") do |link_name, area_name|
  within "##{area_name.downcase.gsub(/ /,'_')}" do
    click_on link_name
  end
end

# Navigation
When('I navigate to the {string} page') do |navigation|

  visit case navigation.downcase
    when 'home' then root_path
    when 'dashboard' then dashboard_path
    else send :"#{navigation.downcase.gsub(/ /,'_')}_path"
  end

end

When('I navigate to the {string} page with the {string} action type') do |navigation, action_type_name|
  case navigation.downcase
  when 'home' then root_path
  when 'dashboard' then dashboard_path
  else page = "#{navigation.downcase.gsub(/ /,'_')}_path"

  visit send(:"#{page}", action_type: proper_object_name(action_type_name))
end

end

When('I navigate/go to the {string} {int} {string} page') do |resource_name, resource_id, action_type|

  local_resource_name = resource_name.downcase.gsub(/ /,'_')
  
  locate_resource = generic_assign_variable(local_resource_name, resource_id)

  if local_resource_name.include? '_child'
    page_name = local_resource_name.gsub(/_child/,'')
  else
    page_name = local_resource_name
  end

  page_path = "/#{page_name.pluralize}/#{locate_resource.id}"

  case action_type.downcase
  when 'delete'
    # page.driver.delete page_path
    current_driver = Capybara.current_driver
    Capybara.current_driver = :rack_test
    page.driver.submit :delete, page_path, {}
    Capybara.current_driver = current_driver

  when 'show'
    visit page_path
  else
    visit "#{page_path}/#{action_type.downcase.gsub(/ /,'_')}"
  end

end

When('I navigate to the {string} {int} {string} page for user {int}') do |resource_name, resource_id, action_type, user_id|

  local_resource_name = resource_name.downcase.gsub(/ /,'_')
  
  locate_resource = generic_assign_variable(local_resource_name, resource_id)

  page_path = "/users/#{@users[user_id.to_i - 1].id}/#{local_resource_name.pluralize}/#{locate_resource.id}"

  case action_type.downcase
  when 'delete'
    # page.driver.delete page_path
    current_driver = Capybara.current_driver
    Capybara.current_driver = :rack_test
    page.driver.submit :delete, page_path, {}
    Capybara.current_driver = current_driver

  else
    visit page_path
  end

end

When('I navigate to the {string} {int} {string} {int} {string} page') do |resource_1_name, resource_1_id, resource_2_name, resource_2_id, action_type|

  resource_1_object = generic_assign_variable(resource_1_name, resource_1_id)
  resource_2_object = generic_assign_variable(resource_2_name, resource_2_id)

  page_path = "/#{resource_1_object.class.to_s.split('::').first.underscore.pluralize}/#{resource_1_object.id}/" +
                "#{resource_2_object.class.to_s.split('::').first.underscore.pluralize}/#{resource_2_object.id}/"

  if action_type.downcase == 'edit'
    page_path += 'edit'
  end
  
  visit_page page_path, action_type

end

When('I navigate to my {string} page') do |navigation|

  visit case navigation.downcase
    when 'home' then root_path
    when 'dashboard' then dashboard_path
    else send(:"#{navigation.downcase.gsub(/ /,'_')}_path", @user.id)
  end

end