When('I fill in the {string} in mind {string} in the {string} {string} {string} selector') do
  |resource_name, resource_property, selector_name, selector_type, selector_category|

  lookup_object = generic_assign_variable(resource_name, 0, true)
  lookup_object_value = lookup_object.send(:"#{proper_object_name(resource_property)}")

  fill_in_flatpickr_date(proper_selector_name(selector_name, selector_type), selector_category, with: lookup_object_value)
end

When('I fill in the date value of {string} in the {string} {string} {string} selector') do
  |date_value, selector_name, selector_type, selector_category|

  case date_value
  when 'today'
    select_date_value = Date.today
  else
    select_date_value = date_value.to_date
  end

  fill_in_flatpickr_date(proper_selector_name(selector_name, selector_type), selector_category, with: select_date_value)
end
