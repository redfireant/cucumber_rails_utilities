Then('I will be on the {string} page') do |page_name|
  expect(page).to case page_name.downcase
  when 'home'
    have_title(ENV['application_name'])
  when 'current user profile'
    have_title("#{@user.email} Profile Page".titleize)
  else
    have_title(page_name.titlecase)
  end
end

Then(/^I cannot see the "([^"]*)"/) do |navigation_name|
  expect(page).not_to have_css("##{navigation_name.downcase.gsub(/ /,'_')}")
end

Then('I cannot visit the {string} page') do |navigation_page|
  visit(send :"#{navigation_page.downcase.gsub(/ /,'_')}_path")

  within 'div#alert-box' do
    expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}
  end

end

Then('I cannot visit the {string} {int} {string} page') do
  |object_name, object_number, page_name|

  compare_value = generic_assign_variable(object_name, object_number)

  case page_name.downcase
  when 'show', 'delete'
    page_path = "#{proper_object_name(object_name.singularize)}"
  when 'edit'
    page_path = "edit_#{proper_object_name(object_name.singularize)}"
  else
    raise 'Invalid page type'
  end

  if page_name.downcase == 'delete'
    page.driver.submit :delete, send(:"#{page_path}_path", compare_value.id), {}
  else
    visit (send :"#{page_path}_path", compare_value.id)
  end

  expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}
end

Then('I cannot visit the {string} page of the {string} {int} attached to {string} {int}') do
  |page_name, child_resource_name, child_id, parent_resource_name, parent_id|

  child_object = generic_assign_variable(child_resource_name, child_id)
  parent_object = generic_assign_variable(parent_resource_name, parent_id)

  case page_name
  when 'show'
    page_path = "#{proper_object_name(parent_resource_name.singularize)}_#{proper_object_name(child_resource_name.singularize)}"
  when
    page_path = "edit_#{proper_object_name(parent_resource_name.singularize)}_#{proper_object_name(child_resource_name.singularize)}"
  else
    raise 'Invalid page type'
  end

  # expect { visit (send :"#{page_name.downcase.gsub(/ /, '_')}_path", compare_value.id) }.to raise_exception(ActiveRecord::RecordNotFound)
  visit (send :"#{page_path}_path", parent_object, child_object)

  expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}

end


Then("{string} {int} cannot be found") do |object_name, object_number|

  locate_resource = generic_assign_variable(object_name, object_number)

  visit_path = "#{locate_resource.class.name.downcase}_path"

  expect { visit (send :"#{visit_path}", locate_resource.id) }.to raise_exception(ActiveRecord::RecordNotFound)

end

Then('I will be on the {string} {int} {string} page') do |object_name, object_number, page_type|

  locate_resource_memory = generic_assign_variable(object_name, object_number)
  locate_resource = generic_assign_object(object_name, locate_resource_memory.id)

  if page_type.include? 'show'
    page_type = 'show'
  end

  case page_type.downcase
  when 'edit'
    page_title = "Edit #{(locate_resource.name.titleize)}"
  when 'show'
    page_title = (locate_resource.name.titleize)
  else
    fail 'Invalid page type'
  end

  expect(page).to have_title(page_title)
end


Then("I cannot access the {string} path from any of the {string} from the {string} in position {int}") do |method_name, sub_resource_name, resource_name, position_number|
  resource = generic_assign_variable(resource_name, position_number)
  sub_resources = resource.public_send(sub_resource_name.downcase.pluralize)

  sub_resources.each do |sub_resource|
    visit_path = "#{resource_name.downcase.gsub(/ /,'_')}_path"

    case method_name.downcase
    when 'new', 'edit'
      visit_path = "#{method_name.downcase}_#{resource_name.downcase.gsub(/ /,'_')}_path"
    end

    if method_name.downcase == 'delete'
      page.driver.submit :delete, "/#{sub_resource_name.pluralize.downcase.gsub(/ /,'_')}/#{sub_resource.id}", {}
    else
      visit (send :"#{visit_path}", sub_resource.id)
    end

    expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}

  end

end

Then('I cannot visit the {string} {int} {string} page associated to {string} {int}') do
  |child_resource_name, child_resource_id, action_name, parent_resource_name, parent_resource_id|

  parent_object = generic_assign_variable(parent_resource_name, parent_resource_id)
  child_object = generic_assign_variable(child_resource_name, child_resource_id)

  page_root = "#{proper_object_name(parent_resource_name)}_#{proper_object_name(child_resource_name)}"

  visit_path = compose_page_url(page_root, action_name, [parent_object.id, child_object.id])

  visit visit_path

  expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}

end

Then('I cannot visit the {string} {int} {string} {string} page') do
  |resource_name, resource_id, resource_property, page_type|

  resource_object = generic_assign_variable(resource_name, resource_id)
  resource_object_property = resource_object.send(:"#{proper_object_name(resource_property)}")

  page_root = "#{proper_object_name(resource_name)}_#{proper_object_name(resource_property)}"

  object_ids = [resource_object.id, resource_object_property.id]

  visit_path = compose_page_url(page_root, page_type, object_ids)

  visit_page visit_path, page_type

end
