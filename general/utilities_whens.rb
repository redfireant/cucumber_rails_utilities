When('I will debug here') do
  binding.break
end

Given('I wait/pause for {int} second(s)') do |seconds|
  sleep seconds.to_i
end