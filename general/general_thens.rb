# Basic page element checking
Then('I {string} see {string} {string} on the page') do |intention_type, selector_id, selector_type|
  check_for_values_in_selectors(intention_type, '', selector_id, selector_type)
end

Then('I {string} see {string} in the {string} {string} section/field/selector on the page') do
  |intention_type, compare_string, selector_id, selector_type|

  check_for_values_in_selectors(intention_type, compare_string, selector_id, selector_type)
end

# Checking to see values from an object array in selectors within the page
Then('I {string} see the {string} {int} {string} {string} selector in the {string} {int} {string} {string} selector') do
  |intent_type, search_object_name, search_object_id, search_object_desc, search_selector_type,
    container_object_name, container_object_id, container_object_desc, container_selector_type|

  search_resource = generic_assign_variable(search_object_name, search_object_id)
  container_resource = generic_assign_variable(container_object_name, container_object_id)

  within proper_selector_name("#{container_object_name}_#{container_resource.id}_#{container_object_desc}", container_selector_type) do
    expect(body).to have_css(proper_selector_name("#{search_object_name}_#{search_resource.id}_#{search_object_desc}", search_selector_type))
  end
end

Then('I {string} see {string} {int} {string} {string} in the {string} {string} selector') do
  |intent_type, search_resource_name, search_resource_id, search_resource_property, search_type, selector_name, selector_type|

  search_object = generic_assign_variable(search_resource_name, search_resource_id)
  search_object_property = search_object.send(proper_object_name(search_resource_property))
  selector = proper_selector_name(selector_name, selector_type)

  if search_type == 'exactly'
    exactness_flag = true
  else
    exactness_flag = false
  end

  case intent_type.downcase
  when 'will not', "won't"
    expect(body).not_to have_selector(selector, text: search_object_property, exact_text: exactness_flag)
  when 'will'
    expect(body).to have_selector(selector, text: search_object_property, exact_text: exactness_flag)
  end

end

# Can click on a string within a selector

Then('I can click on the {string} within the {string} {string} selector') do |link_name, container_name, selector_type|
  within proper_selector_name(container_name, selector_type) do
    expect(page).to have_selector(proper_selector_name(link_name,'id'))
  end
end

Then("I can click on the {string} {int} {string} in the {string} on the page") do |object_name, object_id, object_field, page_element_id|
  compare_value = generic_assign_variable(object_name, object_id)
  object_field_name = object_field.downcase.gsub(/ /,'_')

  page_element_id_name = page_element_id.downcase.gsub(/ /,'_')
  compare_value_text = compare_value.send(object_field_name)

  expect(page).to have_selector("##{page_element_id_name}", text: compare_value_text)

end

Then('I can click on {string} {string} in the {string} within the {string} section') do |resource_name, resource_property, page_element_id, section_name|

  if resource_name == 'my'
    compare_value = generic_assign_variable('user', 0, true)
  else
    compare_value = generic_assign_variable(resource_name, 0, true)
  end

  compare_value_text = compare_value.send(resource_property.gsub(/ /,'_'))

  within proper_selector_name(section_name, 'class') do
    expect(page).to have_selector(proper_selector_name(page_element_id, 'id'), text: compare_value_text)
  end

end

# Checking for delete functions
Then("I cannot delete {string} {int}") do |object_name, object_number|
  locate_resource = generic_assign_variable(object_name, object_number)

  page.driver.submit :delete, (send(:"#{locate_resource.class.name.downcase}_path", locate_resource.id)), {}

  expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}

end

# Checking for order
Then("I will see {string} before {string} in {string}") do |before_text, after_text, page_section|
  within "##{page_section.downcase.gsub(/ /,'_')}" do
    expect(before_text).to appear_before(after_text)
  end
end

Then("I will see {string} before {string} {int} in the {string}") do |before_text, after_resource, after_id, page_section|

  locate_resource = generic_assign_variable(after_resource, after_id)
  after_text = locate_resource.name

  within "##{page_section.downcase.gsub(/ /,'_')}" do
    expect(before_text).to appear_before(after_text.to_s)
  end

end

Then("I will see {string} {int} before {string} {int} in the {string}") do |before_resource, before_id, after_resource, after_id, page_section|

  locate_resource = generic_assign_variable(before_resource, before_id)
  before_text = locate_resource.name


  locate_resource = generic_assign_variable(after_resource, after_id)
  after_text = locate_resource.name

  within "##{page_section.downcase.gsub(/ /,'_')}" do
    expect(before_text).to appear_before(after_text.to_s)
  end

end

Then("I will see {string} except {string} shown in the {string} {string} is ordered alphabetically") do |resource_name, exception_name, area_name, area_type|

  resource = generic_resource_name(resource_name)

  selector_name = proper_selector_name(area_name, area_type)
  before_text = ''

  resource.sort_by(&:name).each do |resource_item|
    if before_text.empty?
      within selector_name do
        expect(before_text).to appear_before(resource_item.name)
      end
    end

    before_text = resource_item.name

  end

  expect(before_text).to appear_before('Other')

end

Then('I {string} see {string} {string} in the {string} {string} on the page') do
  |intention_type, resource_name, resource_property, selector_name, selector_type|

  if resource_name == 'my'
    compare_value = generic_assign_variable('my', nil)
  else
    compare_value = generic_assign_variable(resource_name, nil)
  end

  search_text = compare_value.send(resource_property.gsub(/ /,'_'))

  check_for_values_within_section(intention_type, search_text, selector_name, selector_type)

end

Then('I will see all of the {string} in the {string} {string} selector') do |resource_name, selector_name, selector_type|

  compare_resource = generic_resource_name(resource_name)

  within proper_selector_name(selector_name, selector_type) do
    compare_resource.each do |resource|
      expect(page).to have_text(resource.name)
    end
  end

end

Then('I will see the count of all of the {string} as {string}') do |resource_name, selector_name|

  compare_resource = generic_resource_name(resource_name)
  expect(page).to have_selector(".#{selector_name.singularize.downcase.gsub(/ /,'-')}", count: compare_resource.count)

end

Then('I will see all of my {string} in the {string} {string}') do |method_name, selector_name, selector_type|

  values = @user.send(:"#{method_name.downcase.gsub(/ /,'_')}")

  values.each do |value|
    within proper_selector_name(selector_name, selector_type) do
      expect(page).to have_text(value.name)
    end
  end

end

Then('I will see {int} {string} {string} within the {string} {string} on the page') do
  |num_of_items, selector_name, selector_type, region_selector_name, region_selector_type|

  selected_selector_name = proper_selector_name(selector_name.singularize, selector_type)

  within proper_selector_name(region_selector_name, region_selector_type) do
    expect(page).to have_selector(selected_selector_name, count: num_of_items)
  end

end

Then('I will see {int} {string} in the {string} {int} {string} {string} selector') do
  |num_of_items, check_resource, container_resource_name, container_resource_id, selector_name, selector_type|

  container_object = generic_assign_variable(container_resource_name, container_resource_id)

  within proper_selector_name("#{container_resource_name} #{container_object.id} #{selector_name}", selector_type) do
    expect(page).to have_css(proper_selector_name(check_resource.singularize, 'class'), count: num_of_items)
  end

end

# Check for element state
Then('the {string} {string} within the {string} {string} is {string}') do |selector_id, selector_type, region_id, region_type, selector_state|

  if selector_state == 'disabled'
    disabled_state = true
  elsif selector_state == 'enabled'
    disabled_state = false
  else
    raise 'Not a valid selector state'
  end

  selector_name = proper_selector_name(selector_id, selector_type)

  within proper_selector_name(region_id, region_type) do
    expect(page).to have_button(selector_id, disabled: disabled_state)
  end
end

Then('I will see all {string} associated with {string} {int} on the page') do
  |sub_resource_name, check_resource_name, check_resource_id|

  resource = generic_assign_variable(check_resource_name, check_resource_id)

  sub_resource = resource.send(sub_resource_name.downcase.gsub(/ /,'_'))

  sub_resource.each do |sub_item|
    expect(page).to have_text(sub_item.name)
  end

  # pending # Write code here that turns the phrase above into concrete actions
end
