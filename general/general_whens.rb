# Select values from a dropdown
When("I select {string} {int} from the {string} on the page") do 
  |object_name, object_number, page_element|

  locate_resource = generic_assign_variable(object_name, object_number)

  page_element_name = "#{page_element.downcase.gsub(/ /,'_')}"

  within "##{page_element_name}" do
    find("option[value='#{locate_resource.id}']").select_option
  end

  # page.execute_script("$('##{page_element_name}').val('#{locate_resource.id}')")

end

When('I select {string} {int} {string} from the {string} on the page') do 
  |resource_name, resource_id, resource_field, selector_name|

  locate_resource = generic_assign_variable(resource_name, resource_id)

  select_value = locate_resource.send(:"#{proper_object_name(resource_field)}")

  page_element_name = proper_selector_name(selector_name, 'id')

  find("select#{page_element_name} option", text: select_value).select_option
end

When('I select {string} from the {string} {string} selector on the page') do |value_name, selector_name, selector_type|

  page.select value_name, from: proper_selector_name(selector_name, selector_type)

end

When("I fill in {string} in the {string} field") do |fill_content, field_name|
  fill_in "#{field_name.downcase.gsub(/ /,'_')}", with: fill_content
end

When("I fill in {string} {int} {string} in the {string} field on the page") do |object_name, object_id, object_field_name, page_element|
  locate_resource = generic_assign_variable(object_name, object_id)
  page_element_name = page_element.downcase.gsub(/ /,'_')

  compare_value_text = locate_resource.send(object_field_name.downcase.gsub(/ /,'_'))

  fill_in page_element_name, with: compare_value_text
end

When('I fill in the date value of {string} in the {string} {string} on the page') do |entry_date, selector_name, selector_type|

  case entry_date
  when 'today'
    fill_value = Date.today
  when 'tomorrow'
    fill_value = Date.tomorrow
  else
    fill_value = entry_date
  end

  fill_in_simple_form_date proper_selector_name(selector_name, 'selector'), with: fill_value

end

When('I fill in the {string} value of {string} in the {string} {int} {string} {string} field') do 
  |object_type, object_value, resource_name, resource_id, resource_descriptor, selector_type|
  
  case object_type.downcase
  when 'text'
    compare_value_text = object_value
  when 'date'
    compare_value_text = Date.parse(object_value)
  else
    compare_value_text = object_value
  end

  page_element_name = proper_selector_name("#{resource_name} #{resource_id} #{resource_descriptor}", 'selector')

  fill_in page_element_name, with: compare_value_text
  
  # pending # Write code here that turns the phrase above into concrete actions
end


When('I delete {string} {int}') do |resource, resource_id|

  located_object = generic_assign_variable(resource, resource_id)

  path = "/#{resource.pluralize.downcase.gsub(/ /,'_')}/#{located_object.id}"

  current_driver = Capybara.current_driver
  Capybara.current_driver = :rack_test
  # page.driver.submit :delete, path, {}
  page.driver.delete(path)
  Capybara.current_driver = current_driver  
end

# When('I fill in the {string} value of {string} in the {string} {int} {string} {string} field') do 
#   |value_type, value, resource_name, resource_id, selector_description, selector_type|

#   object = generic_assign_variable(resource_name, resource_id)

#   selector_name = proper_selector_name("#{resource_name} #{object.id} #{selector_description}", 'selector')

#   fill_in selector_name, with: value
# end

When('I fill in the {string} in mind {string} in the {string} {string} selector with the {string} type') do 
  |resource_name, resource_field, selector_name, selector_type, editor_type|

  resource_object = generic_assign_variable(resource_name, 0, true)
  lookup_value = resource_object.send(proper_object_name(resource_field))

  fill_selector = proper_selector_name(selector_name, selector_type)

  case editor_type
  when 'tinymce'
    fill_in_tinymce_editor(fill_selector, with: lookup_value)
  when 'trix'
    fill_in_trix_editor(fill_selector, with: lookup_value)
  else
    fail "#{library_type} is not a supported library type"
  end

end