Given('I have a fully seeded system for stuff') do
  DataLoaderGeneral.load_seed_data
end


Given("the application key {string} has the configuration of {string}") do |config_key, config_value|
  ENV[config_key] = config_value
end

Given('I have {int} {string} in the system') do |num_of_objects, resource_name|

  local_resource_array = generic_resource_name(resource_name)

  if local_resource_array.nil?
    local_resource_array = []
  end

  formal_object_name = proper_object_name(resource_name)

  (0..(num_of_objects.to_i - 1)).each do
    local_object = FactoryBot.create(:"#{proper_object_name(formal_object_name.singularize)}", user: @user)
    local_resource_array << local_object
  end

  instance_variable_set("@#{formal_object_name.pluralize}", local_resource_array)

end

Given('the {string} {int} {string} property has the {string} value of {string}') do
  |resource_name, resource_id, resource_property, object_type, object_value|

  locate_resource = generic_assign_variable(resource_name, resource_id)
  property_name = resource_property.downcase.gsub(/ /,'_')

  assign_value_to_object_property(locate_resource, resource_property, object_type, object_value)

  locate_resource.save

end

Given('there are/is {int} {string} in the system') do |num_of_objects, object_name|
  local_resource_array = []
  formal_object_name = proper_object_name(object_name.singularize)

  (0..(num_of_objects.to_i - 1)).each do
    local_object = FactoryBot.create(:"#{formal_object_name}")
    local_resource_array << local_object
  end

  instance_variable_set("@#{formal_object_name.pluralize}", local_resource_array)

end

Given('there are {int} {string} with the {string} trait in the system') do
  |num_of_objects, object_name, object_trait|

  local_resource_array = []
  formal_object_name = proper_object_name(object_name)

  (0..(num_of_objects.to_i - 1)).each do
    local_object = FactoryBot.create(:"#{proper_object_name(object_trait.singularize)}_#{proper_object_name(object_name.singularize)}")
    local_resource_array << local_object
  end

  instance_variable_set("@#{formal_object_name.pluralize}", local_resource_array)

end

Given('there is/are {int} {string} associated with/to {string} {int} in the system') do
  |num_of_resources, resource_name, associated_resource_name, associated_resource_number|

  parent_resource = generic_assign_variable(associated_resource_name.gsub(/ children/,''),associated_resource_number)

  local_resource_array = generic_resource_name(resource_name.pluralize)
  local_resource_array = [] if local_resource_array.nil?

  formal_object_name = resource_name.singularize.downcase.gsub(/ /,'_')

  (0..(num_of_resources.to_i - 1)).each do
    local_object = FactoryBot.create(:"#{formal_object_name}", :"#{proper_object_name(associated_resource_name.gsub(/ children/,''))}_id" => parent_resource.id)
    local_resource_array << local_object
  end

  instance_variable_set("@#{formal_object_name.pluralize}", local_resource_array)

end

Given('{string} {int} {string} is associated to the {string} of {string} {int}') do
  |associated_object_name, associated_object_id, associated_field_name,
    parent_object_field, parent_object_name, parent_object_id|

    parent_resource = generic_assign_variable(parent_object_name, parent_object_id)
    associated_resource = generic_assign_variable(associated_object_name, associated_object_id)

    parent_resource.send("#{proper_object_name(parent_object_field)}=", associated_resource.send(proper_object_name(associated_field_name)))
    parent_resource.save
end

Given('a {string} is associated to the {string} of {string} {int} in the system') do |new_resource_name, property, resource_name, resource_id|

  new_object = FactoryBot.create(proper_object_name(new_resource_name))

  resource_object = generic_assign_variable(resource_name, resource_id)
  resource_object.send("#{proper_object_name(property)}=", new_object)
  resource_object.save
end

Given('the {string} {int} {string} property has a date value of {int} day(s) from now') do
  |resource_name, resource_id, resource_property, days_from_now|

  local_object = generic_assign_variable(resource_name, resource_id)

  local_object.send("#{proper_object_name(resource_property)}=", Date.today + days_from_now.to_i.days)

end

Given('every {string} {string} is associated with the {string} property of my {string}') do
  |object_list, object_property, associated_resource_subproperty, associated_resource_property|

  associated_properties = @user.send(proper_object_name(associated_resource_property))
  associated_properties_array = associated_properties.shuffle

  object_array = generic_resource_name(object_list)

  object_array.each do |object|
    object.send("#{proper_object_name(object_property)}=", associated_properties_array.pop.send(proper_object_name(associated_resource_subproperty)))
    object.save
  end
end

Given('every {string} {string} is associated with the {string} {string} property of {string} {int}') do
  |object_list, object_property, associated_resource_property, associated_resource_subproperty, resource_name, resource_id|

  resource_object = generic_assign_variable(resource_name, resource_id)
  associated_properties = resource_object.send(proper_object_name(associated_resource_property))

  object_array = generic_resource_name(object_list)

  object_array.each do |object|
    object.send("#{proper_object_name(object_property)}=", associated_properties.sample.send(proper_object_name(associated_resource_subproperty)))
    object.save
  end

end
