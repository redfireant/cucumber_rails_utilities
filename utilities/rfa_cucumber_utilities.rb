def extract_selector_name(selector_name)
  case selector_name.singularize
  when / child/
    return selector_name.singularize.gsub(/ child/, '')
  when / grandchild/
    return selector_name.singularize.gsub(/ grandchild/, '')    
  else
    return selector_name.singularize
  end
end

def proper_selector_name(selector_name, selector_type)
  case selector_type.singularize.downcase
  when 'class'
    full_selector_name = ".#{selector_name.downcase.gsub(/ /,'-')}"
  when 'id'
    full_selector_name = "##{selector_name.downcase.gsub(/ /,'_')}"
  when 'selector', 'general', 'field'
    full_selector_name = "#{selector_name.downcase.gsub(/ /,'_')}"
  else
    fail 'Invalid selector type'
  end

  return full_selector_name
  
end

# Returns an object name conforming to Rails standards
def proper_object_name(object_name)
  return object_name.downcase.gsub(/ /,'_')
end

# Returns the object with the same name
def generic_resource_name(object_name)
  ruby_variable_name = object_name.downcase.gsub(/ /,'_')

  if ruby_variable_name.downcase == 'stuff'
    ruby_variable_name = "@#{ruby_variable_name}"
  else
    ruby_variable_name = "@#{ruby_variable_name.pluralize}"
  end

  local_variable = instance_variable_get("#{ruby_variable_name}")

  if local_variable.nil?
    local_variable = instance_variable_set("#{ruby_variable_name}", [])
  end
  
  return local_variable
end

# Locate an object within the database
def generic_assign_object(object_name, object_number)

  # Parse out children
  
  clean_object_name = object_name.downcase.gsub(/ child/, '')
  ruby_resource_name = clean_object_name.titleize.gsub(/\s+/, "")

  if ruby_resource_name.downcase.include?('stuff')
    ruby_resource_name = ruby_resource_name.singularize
  end

  resource = ruby_resource_name.constantize

  locate_resource = resource.find(object_number)

  return locate_resource

end

# Locate the object through the name propery
def generic_assign_object_with_name(resource_name, lookup_value)

  lookup_resource = proper_object_name(resource_name).camelize.constantize

  locate_object = lookup_resource.find_by(name: lookup_value)

  return locate_object
  
end

# Locate an object within an array
# This only points to the object in memory and is not a reflection of the value in the database
# The in_mind flag is used to leverage the singular object generally used for resources "in mind".
# For this to work, you also have to pass in 0 as the object_number
def generic_assign_variable(object_name, object_number, in_mind = false)

  ruby_resource_name = object_name.gsub(/\s+/, "")
  ruby_variable_name = object_name.downcase.gsub(/ /,'_')

  if in_mind
    local_variable = instance_variable_get("@#{ruby_variable_name}")
  elsif object_name == 'my'
    local_variable = instance_variable_get("@user")
  else
    local_variable = instance_variable_get("@#{ruby_variable_name.pluralize}")
  end

  if in_mind || (object_name == 'my')
    return local_variable
  else
    return local_variable[object_number.to_i - 1]
  end

end

# Check values with intention
def check_for_values_within_section(intent_type, compare_value, selector_name, selector_type)
  within proper_selector_name(selector_name, selector_type) do
    case intent_type.downcase
    when 'will'
      expect(page).to have_text(compare_value)
    when 'will not', "won't"
      expect(page).not_to have_text(compare_value)
    else
      raise "Invalid intent type. Only supports will, will not or won't"
    end
  end
end

# Check selectors with intention
def check_for_selectors(intent_type, find_selector_name, find_selector_type)
    case intent_type.downcase
    when 'will'
      expect(body).to have_selector(proper_selector_name(find_selector_name, find_selector_type))
    when 'will not', "won't"
      expect(body).not_to have_selector(proper_selector_name(find_selector_name, find_selector_type))
    else
      raise "Invalid intent type. Only supports will, will not or won't"
    end  
end

# Check for values in selectors with intention
def check_for_values_in_selectors(intent_type, compare_value, selector_name, selector_type)
  case intent_type.downcase
  when 'will'
    if compare_value.blank?
      expect(page).to have_selector(proper_selector_name(selector_name, selector_type))
    else
      expect(page).to have_selector(proper_selector_name(selector_name, selector_type), text: compare_value)
    end
  when 'will not', "won't"
    if compare_value.blank?
      expect(page).not_to have_selector(proper_selector_name(selector_name, selector_type))
    else
      expect(page).not_to have_selector(proper_selector_name(selector_name, selector_type), text: compare_value)
    end
  else
    raise "Invalid intent type. Only supports will, will not or won't"
  end
end

# Create nodes and associate it to parent nodes
def associate_nodes_to_parents(num_of_children, resource_object)

  local_resource_array = []
  formal_object_name = resource_object.class.name.underscore

  (0..(num_of_children.to_i - 1)).each do
    child_object =  FactoryBot.create(:"#{formal_object_name}", parent: resource_object)
    local_resource_array << child_object
  end

  return local_resource_array
  
end

def create_parent_node(resource_name)

  resource_object = resource_name.singularize.titlecase.gsub(/ /,'').constantize
  if resource_object.roots.count > 0
    parent_node = resource_object.roots.first
  else
    parent_node = FactoryBot.create(:"#{resource_name.downcase.gsub(/ /,'_')}")
  end

  carry_test_object = generic_resource_name(resource_name)

  if carry_test_object.nil?
    carry_test_object = []
  end

  carry_test_object << parent_node
  instance_variable_set("@#{proper_object_name(resource_name.pluralize)}", carry_test_object)
  
  return parent_node
    
end

def assign_value_to_variable(resource_name, resource_id, resource_property, property_value_type, property_value)
  
  if resource_id == 0
    locate_resource = generic_assign_variable(resource_name, 0, true)
  else
    locate_resource = generic_assign_variable(resource_name, resource_id)
  end

  assign_value_to_object_property(locate_resource, resource_property, property_value_type, property_value)

  locate_resource.save

end

def assign_value_to_object_property (locate_resource, resource_property, value_type, object_value)
  
  property_name = proper_object_name(resource_property)

  case value_type.downcase
  when 'string'
    locate_resource.send(:"#{property_name}=", object_value.to_s)

  when 'integer'
    locate_resource.send(:"#{property_name}=", object_value.to_i)

  when 'boolean'
    case object_value.downcase
    when 'false' 
      locate_resource.send(:"#{property_name}=", false)
    when 'true'
      locate_resource.send(:"#{property_name}=", true)
    end

  when 'date'
    locate_resource.send(:"#{property_name}=", object_value)
    
  when 'general'
    case object_value.downcase
    when 'nil'
      locate_resource.send(:"#{property_name}=", nil)
    else
      raise 'Invalid object value'
    end

  else
    begin
      object_find = value_type.camelcase.constantize.find_by(name: object_value)
      locate_resource.send(:"#{property_name}=", object_find.id)
    rescue => exception
      case exception.class.name
      when 'NameError'
        fail "#{object_type.camelcase} is not a valid Class type"
      else
        fail exception
      end
    end
  end

end

# Translate object to RFA standards
def value_translate(object)

  case object.class.name
  when 'Date'
    check_value = object.strftime('%b %d, %Y')
  else
    check_value = object
  end

  return check_value

end