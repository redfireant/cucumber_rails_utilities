
# Checks the page type
# Parameter:
#  locate_resource
#  page_type

def page_check(locate_resource, page_type)

  if page_type.include? 'show'
    page_type = 'show'
  end

  case page_type.downcase
  when 'edit'
    page_title = "Edit #{(locate_resource.name.titleize)}"
  when 'show'
    page_title = (locate_resource.name.titleize)
  else
    fail 'Invalid page type'
  end
  
  expect(page).to have_title(page_title)
  
end

def visit_page(page_path, action_type)

  case action_type.downcase
  when 'delete'
    # page.driver.delete page_path
    current_driver = Capybara.current_driver
    Capybara.current_driver = :rack_test
    page.driver.submit :delete, page_path, {}
    Capybara.current_driver = current_driver

  when 'show', 'edit'
    visit page_path
    
  else
    raise "Invalid action type: #{action_type}"
  end  

end

def compose_page_url(page_root, action_type, object_ids)
  case action_type.downcase
  when 'show', delete
    visit_path = "#{page_root}_path"
  when 'edit'
    visit_path = "edit_#{page_root}_path"
  else
    fail 'Invalid action name'    
  end

  if object_ids.class.name.downcase == 'array'
    case object_ids.count
    when 2
      page_url = send(:"#{visit_path}", object_ids[0], object_ids[1])
    when 1
      page_url = send(:"#{visit_path}", object_ids[0])
    else
      page_url = send(:"#{visit_path}")
    end
  else
    page_url = send(:"#{visit_path}", object_ids)
  end

  return page_url
  
end