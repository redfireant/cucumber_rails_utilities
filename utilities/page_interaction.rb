# Use the right method to fill in the property on the screen
def fill_in_value(fill_value, selector_name)
  case fill_value.class.name.downcase
  when 'string', 'bigdecimal'
    fill_in proper_selector_name(selector_name, 'selector'), with: fill_value
  when 'date'
    fill_in_simple_form_date proper_selector_name(selector_name, 'selector'), with: fill_value
  when 'actiontext::richtext'
    fill_in_trix_editor proper_selector_name(selector_name, 'selector'), with: fill_value
  else
    fail "#{fill_value.class.name} is an unhandled class type"
  end
end

def fill_in_trix_editor(field_name, with:)
  find(:css, "#{proper_selector_name(field_name,'id')}").click.set(with.to_plain_text)
end

def fill_in_tinymce_editor(field_name, with:)
  element = proper_selector_name(field_name,'selector')
  within_frame("#{element}_ifr") do
    page.driver.browser.find_element(:id, 'tinymce').send_keys(with)
  end

end

def fill_in_simple_form_date(field_name, with:)
  date_fill = with.to_date

  select(date_fill.year, from: "#{field_name}_1i" )
  select(date_fill.strftime('%B'), from: "#{field_name}_2i" )
  select(date_fill.day, from: "#{field_name}_3i" )
end

def fill_in_flatpickr_date(field_name, flatpickr_configuration, with:)
  datepicker_type = proper_selector_name(flatpickr_configuration,'selector')
  js = "document.querySelector('#{field_name}')._flatpickr.setDate('#{with}')"
  execute_script(js)
  find('#application_name_text_link').click
end
