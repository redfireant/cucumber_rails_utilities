Then("I will see {string} in the {string} {string} on the page") do |string_value, field_name, field_type|
  within proper_selector_name(field_name,field_type) do
    expect(body).to have_text(string_value.to_s)
  end
end

Given('{string} {int} is assigned to the {string} of {string} {int}') do 
  |associated_resource, associated_resource_id, property_name, resource, resource_id|

  associated_object = generic_resource_name(associated_resource)[associated_resource_id - 1]
  resource_object = generic_resource_name(resource)[resource_id - 1]

  resource_object.send("#{property_name.gsub(/ /,'_')}=", associated_object)

  resource_object.save

end

Then("I cannot visit the {string} page for {string} {int}") do |page_name, object_name, object_number|
  compare_value = generic_assign_variable(object_name, object_number)

  # expect { visit (send :"#{page_name.downcase.gsub(/ /, '_')}_path", compare_value.id) }.to raise_exception(ActiveRecord::RecordNotFound)
  visit (send :"#{page_name.downcase.gsub(/ /, '_')}_path", compare_value.id)

  expect(page).to satisfy {|page| page.has_content?("You are not authorized to access this page.") || page.has_content?("You need to sign in or sign up before continuing.")}

end