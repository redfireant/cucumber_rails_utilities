# basic selector checks
Then('I {string} see the {string} {string} selector') do |intent_type, selector_name, selector_type|
  check_for_selectors(intent_type, selector_name, selector_type)
end

# check to find selectors on the page without objects
Then('I {string} see {string} in the {string} {string} selector') do |intent_type, compare_value, selector_name, selector_type|
  check_for_values_within_section(intent_type, compare_value, selector_name, selector_type)
end

Then('I {string} see the {string} {string} {string} in the {string} {string} selector') do \
  |intent_type, resource_name, sub_resource_name, field_name, selector_name, selector_type|

  if resource_name == 'my'
    resource_object = generic_assign_variable('user', 0, true)
  else
    resource_object = generic_assign_variable(resource_name, 0, true)
  end

  compare_value = resource_object.send(:"#{sub_resource_name.downcase.gsub(/ /,'_')}").send(field_name.downcase.gsub(/ /,'_'))
  check_for_values_within_section(intent_type, compare_value, selector_name, selector_type)

end

Then('I {string} see the {string} {string} selector within the {string} {string} selector on the page') do 
  |intent_type, find_selector_name, find_selector_type, within_selector_name, within_selector_type|

  within proper_selector_name(within_selector_name, within_selector_type) do
    case intent_type.downcase
    when 'will'
      expect(body).to have_selector(proper_selector_name(find_selector_name, find_selector_type))
    when 'will not', "won't"
      expect(body).not_to have_selector(proper_selector_name(find_selector_name, find_selector_type))
    end
      
  end
end

Then("I will not see any {string} class on the screen") do |class_name|
  expect(body).not_to have_css(".#{class_name.downcase.gsub(/ /,'-')}")
end

# Check to find selectors associates with object names

Then('I {string} see the {string} {int} {string}') do |intent_type, resource_name, resource_id, url_descriptor|

  find_object = generic_assign_variable(resource_name, resource_id)
  find_value = proper_object_name( "#{resource_name} #{find_object.id} #{url_descriptor}" )
  check_for_selectors(intent_type, find_value, 'id')
end

# Counting number of elements on the page
Then("I will see {int} {string} on the page") do |num_of_items, item_type|
  expect(page).to have_selector(".#{item_type.singularize.downcase.gsub(/ /,'-')}", count: num_of_items)
end

Then('I will see {int} {string} in the {string} {string} selector') do |num_of_items, item_type, selector_name, selector_type|

  within proper_selector_name(selector_name, selector_type) do
    expect(body).to have_css(proper_selector_name(item_type.singularize, 'class'), count: num_of_items)
  end

end
