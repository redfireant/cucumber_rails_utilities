# Checking for texts in alert messages
Then('I will see {string} on the page') do |message|
  expect(body).to have_text(message)
end

Then('I will see the {string} alert message') do |alert_message|
  within ".alert-message" do
    expect(body).to have_text(alert_message.to_s)
  end
end

Then('I will see {string} in a(n) {string} alert message') do |alert_message, alert_type|

  case alert_type.downcase
  when 'danger', 'error'
    selector_name = proper_selector_name('alert-danger', 'class')
  when 'notice'
    selector_name = proper_selector_name('alert-notice', 'class')
  else
    fail 'Invalid alert type'
  end

  within selector_name do
    expect(body).to have_text(alert_message.to_s)
  end

end

Then("I will see the {string} {string} {int} alert message") do |message_string, object_name, object_number|
  locate_resource = generic_assign_variable(object_name, object_number)

  alert_message = "#{message_string} #{locate_resource.name} #{locate_resource.class.name.downcase}"

  within ".alert" do
    expect(body).to have_text(alert_message.to_s)
  end

end

Then("I will see {string} from position {int} {string} in the alert message") do |resource_name, resource_position_number, required_text|

  resource = generic_assign_variable(resource_name,resource_position_number)

  alert_message = "#{resource.name} #{required_text}"

  within ".alert" do
    expect(body).to have_text(alert_message)
  end
end

Then("I will see {string} in the {string} error message") do |error_message, error_type|
  css_class_name = error_type.downcase.gsub(/ /,'-')
  expect(body).to have_css(".#{css_class_name}", text: error_message)
end

Then('I will see the {string} invalid feedback alert message') do |error_message|
  expect(body).to have_css('.invalid-feedback', text: error_message)
end
